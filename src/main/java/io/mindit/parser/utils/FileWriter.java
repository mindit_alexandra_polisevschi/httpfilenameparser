package io.mindit.parser.utils;

import io.mindit.parser.ParserMain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class FileWriter {

    private static final Logger logger = Logger.getLogger(FileWriter.class.getName());


    public static void writeYml(String targetPath, String content) {
        String target = targetPath + File.separator + "generated-" + System.currentTimeMillis() + ".yml";
        write(target, content);
    }

    public static void writeErrors(String targetPath, String content) {
        String target = targetPath + File.separator + "generated-err" + System.currentTimeMillis() + ".err";
        write(target, content);
    }

    private static void write(String targetPath, String content) {
        Path destination = Paths.get(targetPath);

        try(BufferedWriter writer = Files.newBufferedWriter(destination, Charset.forName("UTF-8"))) {
            writer.write(content);

        } catch(IOException ex) {
            logger.severe("Exception encountered when writing to file " + targetPath + ", " + ex.getMessage());;
        }

        logger.info("Generated file: " + destination.getFileName());
    }
}
