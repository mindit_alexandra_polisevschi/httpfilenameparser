package io.mindit.parser.utils;

import io.mindit.parser.constants.RegexPattern;
import io.mindit.parser.constants.ResultType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilebeatYmlFileGenerator {

    private static final Logger logger = Logger.getLogger(FilebeatYmlFileGenerator.class.getName());

    private static final String[] FILE_SKIPPED = new String[]{"FAILED"};

    /**
     * Sample YML result:     *
     * - type: log
     *   paths:
     *     - /.../apex-test.rbro.rbg.cc-80-access.log*
     *   fields:
     *     virtual_host: apex-test.rbro.rbg.cc
     * 	   port: 80
     * 	   log_type: access
     *
     * Sample ERROR result:
     * Unable to match [filename]. Skipping!!
     */
    public Map<ResultType, String> generateInput(String fileName, String path) {
        Map<ResultType, String> generatedInput = new HashMap<>();

        String[] matched = match(fileName, RegexPattern.first());

        if (Arrays.equals(FILE_SKIPPED, matched)) {
            generatedInput.put(ResultType.ERROR, fileName + System.lineSeparator());
            return generatedInput;
        }

        String result = System.lineSeparator() +
                " - type: log" + System.lineSeparator() +
                "   enabled: true" + System.lineSeparator() +
                "   encoding: utf-8" + System.lineSeparator() +
                "   paths:" + System.lineSeparator() +
                "     - " + path + System.lineSeparator() +
                "   fields:" + System.lineSeparator() +
                "     application: apache-httpd" + System.lineSeparator() +
                "     virtual_host: " + matched[0] + System.lineSeparator() +
                "     port: " + matched[1] + System.lineSeparator() +
                "     log_type: " + matched[2];

        generatedInput.put(ResultType.YML, result);
        return generatedInput;
    }


    private String[] match(String fileName, RegexPattern regexPattern) {
        logger.fine("Parsing " + fileName + " with pattern " + regexPattern.getValue());

        String[] result = new String[3];

        Pattern p = Pattern.compile(regexPattern.getValue());
        Matcher m;

        try {
            m = p.matcher(fileName);
            boolean matchFound = m.matches();
            if (matchFound) {
                result[0] = m.group(1);
                result[1] = m.group(2);
                result[2] = m.group(3);

            } else {
                if (regexPattern.next() != null) {
                    return match(fileName, regexPattern.next());
                }
                return FILE_SKIPPED;
            }

        } catch (Exception e) {
            return FILE_SKIPPED;
        }

        return result;
    }
}
