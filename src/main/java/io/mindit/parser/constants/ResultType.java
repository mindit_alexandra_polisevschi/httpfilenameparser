package io.mindit.parser.constants;

public enum ResultType {

    YML,
    ERROR
}
