package io.mindit.parser.constants;

public enum RegexPattern {

    http_log_3("(\\D+)_(\\d+)_([a-zA-Z]+).log", null),
    http_log_2("(\\D+)_(\\d+)__(\\D+).log", http_log_3),
    http_log_1("([0-9a-zA-Z.-]+)-(\\d+)-(\\D+).log", http_log_2);



    private String value;
    private RegexPattern next;

    RegexPattern(String value, RegexPattern regexPattern) {
        this.value = value;
        this.next = regexPattern;
    }

    public String getValue() {
        return value;
    }

    public static RegexPattern first() {
        return http_log_1;
    }

    public RegexPattern next() {
        return next;
    }
}
