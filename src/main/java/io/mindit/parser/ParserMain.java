package io.mindit.parser;

import io.mindit.parser.constants.ResultType;
import io.mindit.parser.utils.FileWriter;
import io.mindit.parser.utils.FilebeatYmlFileGenerator;

import java.io.File;
import java.util.Map;
import java.util.logging.Logger;

import static java.lang.System.exit;

public class ParserMain {

    private static final Logger logger = Logger.getLogger(ParserMain.class.getName());


    public static void main(String[] args) {

        if (args == null || args.length != 1) {
            logger.severe("Expecting a single argument, the log folder location!");
            exit(0);
        }

        String path = args[0];
        File folder = new File(path);

        File[] files = folder.listFiles();
        if (files == null) {
            logger.severe("No files found in the provided location: " + path);
            exit(0);
        }

        FilebeatYmlFileGenerator ymlFileGenerator = new FilebeatYmlFileGenerator();
        StringBuilder result = new StringBuilder();
        StringBuilder errors = new StringBuilder();

        for (final File fileEntry: files) {
            Map<ResultType, String> generatedInput = ymlFileGenerator.generateInput(fileEntry.getName(), fileEntry.getAbsolutePath());
            result.append(generatedInput.getOrDefault(ResultType.YML, ""));
            errors.append(generatedInput.getOrDefault(ResultType.ERROR, ""));
        }

        FileWriter.writeYml(path, result.toString());
        FileWriter.writeErrors(path, errors.toString());
    }
}
