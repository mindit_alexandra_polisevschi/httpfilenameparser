package io.mindit.parser;

import io.mindit.parser.constants.RegexPattern;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class RegexTest {

    @Test
    public void testRegex_1_source1() {
        String source = "apex-test.rbro.rbg.cc-90-error.log";

        Pattern p = Pattern.compile(RegexPattern.http_log_1.getValue());
        Matcher m = p.matcher(source);

        boolean matchFound = m.matches();
        if (!matchFound) {
            fail("Expected the test pattern to match!");
        }

        assertThat(m.group(1), is("apex-test.rbro.rbg.cc"));
        assertThat(m.group(2), is("90"));
        assertThat(m.group(3), is("error"));
    }

    @Test
    public void testRegex_1_source2() {
        String source = "posta-gp-10.241.30.194-457-access.log";

        Pattern p = Pattern.compile(RegexPattern.http_log_1.getValue());
        Matcher m = p.matcher(source);

        boolean matchFound = m.matches();
        if (!matchFound) {
            fail("Expected the test pattern to match!");
        }

        assertThat(m.group(1), is("posta-gp-10.241.30.194"));
        assertThat(m.group(2), is("457"));
        assertThat(m.group(3), is("access"));
    }


    @Test
    public void testRegex_2() {
        String source = "dmcc-pre.rbro.rbg.cc_443__access.log";

        Pattern p = Pattern.compile(RegexPattern.http_log_2.getValue());
        Matcher m = p.matcher(source);

        boolean matchFound = m.matches();
        if (!matchFound) {
            fail("Expected the test pattern to match!");
        }

        assertThat(m.group(1), is("dmcc-pre.rbro.rbg.cc"));
        assertThat(m.group(2), is("443"));
        assertThat(m.group(3), is("access"));
    }


    @Test
    public void testRegex_3_fails() {
        /* The purpose of this test is to ensure that a pattern with two __ is not identified as a match and,
           as a consequence, parsed incorrectly.
           (e.g. a split like: "mobile.raiffeisenonline.ro', '443', '_error' is not desired).
         */
        String source = "mobile.raiffeisenonline.ro_443__error.log";

        Pattern p = Pattern.compile(RegexPattern.http_log_3.getValue());
        Matcher m = p.matcher(source);

        boolean matchFound = m.matches();
        if (matchFound) {
            fail("Expected the match to fail, since this pattern expects _error and not __error!");
        }
    }

    @Test
    public void testRegex_3_successful() {
        String source = "mobile.raiffeisenonline.ro_443_error.log";

        Pattern p = Pattern.compile(RegexPattern.http_log_3.getValue());
        Matcher m = p.matcher(source);

        boolean matchFound = m.matches();
        if (!matchFound) {
            fail("Expected the test pattern to match!");
        }

        assertThat(m.group(1), is("mobile.raiffeisenonline.ro"));
        assertThat(m.group(2), is("443"));
        assertThat(m.group(3), is("error"));
    }
}
