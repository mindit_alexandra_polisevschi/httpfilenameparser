# httpfilenameparser

This is a small project which generates the content of the &#34;input&#34; section for a filebeat.yml file.

It was created as part of the RZB ELK - HTTPD logs integration project, to generate multiple filebeat inputs and enhance each input with additional fields, generated automatically by parsing the file name.